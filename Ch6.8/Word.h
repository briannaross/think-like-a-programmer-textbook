#pragma once
#include <string>

class Word
{
private:
	struct Letter {
		char c;
		Letter* next;
	};

public:
	Word();
	~Word();

	void AddLetter(char c);
	bool IsPalindrome();
	std::string GetWordForwards();
	std::string GetWordBackwards();


private:
	Letter* head;
	std::string WordForwards(Letter* rec);
	std::string WordBackwards(Letter* rec);

};

