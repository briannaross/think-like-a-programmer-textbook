#include "Word.h"

Word::Word()
{
	head = nullptr;
}


Word::~Word()
{
	Letter* curRec = head;
	while (curRec != nullptr) {
		Letter* thisRec = curRec;
		curRec = curRec->next;
		delete thisRec;
	}
}

void Word::AddLetter(char c)
{
	Letter* newRec = new Letter;
	newRec->c = c;
	newRec->next = nullptr;

	if (head == nullptr) {
		head = newRec;
		return;
	}
	
	Letter* curRec = head;
	while (curRec->next != nullptr) {
		curRec = curRec->next;
	}
	curRec->next = newRec;
}

bool Word::IsPalindrome()
{
	if (head == nullptr) {
		return false;
	}

	Letter *wf = head;
	Letter *wb = head;

	if (WordForwards(wf) == WordBackwards(wb))
		return true;
	else
		return false;
}

std::string Word::GetWordForwards()
{
	Letter *wf = head;
	return WordForwards(wf);
}

std::string Word::GetWordBackwards()
{
	Letter *wb = head;
	return WordBackwards(wb);
}

std::string Word::WordForwards(Letter* rec)
{
	if (rec == nullptr) return "";

	return rec->c + WordForwards(rec->next);
}

std::string Word::WordBackwards(Letter* rec)
{
	if (rec == nullptr) return "";

	return WordBackwards(rec->next) + rec->c;
}