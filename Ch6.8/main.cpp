#include <iostream>
#include "Word.h"

int main()
{
	Word w;

	w.AddLetter('t');
	w.AddLetter('r');
	w.AddLetter('0');
	w.AddLetter('p');
	w.AddLetter('u');
	w.AddLetter('r');
	w.AddLetter('t');


	std::cout << w.GetWordForwards() << std::endl;
	std::cout << w.GetWordBackwards() << std::endl;

	std::string result = w.IsPalindrome() ? "" : "not ";
	std::cout << "Word is " << result << "a palindrome" << std::endl;

	return 0;
}