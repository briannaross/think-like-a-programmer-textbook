#pragma once
class BTree
{
private:
	struct Node {
		int data;
		Node* left;
		Node* right;
	};

public:
	BTree();
	~BTree();
	bool Find(int n);
	void Insert(int n);
	bool Delete(int n);
	bool IsHeap();
	void PrintTree();

private:
	Node* root;
	Node* FindNode(Node* curNode, Node *prevNode, int n);
	void InsertNode(Node* curNode, Node* newNode);
	void DeleteNode(Node* curNode);
	bool CheckIsHeap(Node* curNode);
	void PrintNode(Node* curNode);
};

