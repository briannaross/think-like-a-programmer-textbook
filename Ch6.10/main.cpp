#include <iostream>
#include "BTree.h"

int main()
{
	BTree bt;

	bt.Insert(5);
	bt.Insert(3);
	bt.Insert(7);
	bt.Insert(4);
	bt.Insert(2);
	bt.Insert(6);
	bt.Insert(8);
	bt.Insert(1);
	bt.Insert(9);
	bt.Insert(0);

	bt.PrintTree();

	bt.Find(5) ? (std::cout << "Found!\n") : (std::cout << "Not found\n");

	bt.IsBST() ? (std::cout << "BTree is a binary search tree!\n") : (std::cout << "BTree is not a binary search tree\n");

	bt.Delete(5);
	bt.Delete(2);
	bt.Delete(1);
	bt.Delete(4);
	bt.Delete(6);
	bt.Delete(3);
	bt.Delete(7);
	bt.Delete(0);
	bt.Delete(8);
	bt.Delete(9);

	bt.PrintTree();

	return 0;
}