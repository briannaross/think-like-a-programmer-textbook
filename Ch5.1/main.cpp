#include <iostream>
#include "Automobile.h"

int main()
{
	Automobile a1;

	std::string manufacturer = "Ford";
	std::string model = "Focus";

	a1.SetManufacturer(manufacturer);
	a1.SetModel(model);
	a1.SetYear(2004);

	return 0;
}