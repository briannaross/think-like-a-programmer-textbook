#pragma once
class VLString
{
private:
	struct charNode {
		char c;
		charNode* next;
	};

public:
	VLString();
	VLString(char c);
	~VLString();
	VLString& operator=(const VLString &rhs);
	char operator[](int offset);
	void Append(char c);
	char CharacterAt(int pos);
	void Concatenate(VLString &str2);
	void Remove(int offset, int len);
	void Print();

private:
	typedef charNode* charList;
	charList _listHead;
	void DeleteList(charList listPtr);
	charList CopiedList(const charList original);
};

