#include <iostream>
#include "VLString.h"

VLString::VLString()
{
	_listHead = nullptr;
}

VLString::VLString(char c)
{
	//_listHead = new charNode;
	//_listHead->c = c;
	//_listHead->next = nullptr;
	Append(c);
}

VLString::~VLString()
{
	DeleteList(_listHead);
}

VLString& VLString::operator=(const VLString &rhs)
{
	if (this != &rhs) {
		DeleteList(_listHead);
		_listHead = CopiedList(rhs._listHead);
	}

	return *this;
}

char VLString::operator[](int offset)
{
	charNode* curNode = _listHead;
	int counter = 0;

	while (offset != counter) {
		curNode = curNode->next;
		counter++;
	}

	if (curNode != nullptr) {
		return curNode->c;
	}

	std::cerr << "Offset " << offset << " is higher than number of elements in list." << std::endl;
	return 7; // Bell
}

void VLString::Append(char c)
{
	// Create the new node
	charNode* newNode = new charNode{ c };
	newNode->next = nullptr;

	// Set listhead to the new node if list is empty
	if (_listHead == nullptr) {
		_listHead = newNode;
		return;
	}

	// Create a copy of listhead to cycle through the list
	charNode* curNode = _listHead;

	// Cycle through the list to the end
	while (curNode->next != nullptr) {
		curNode = curNode->next;
	}

	// Set the last node to the new node
	curNode->next = newNode;
}

char VLString::CharacterAt(int pos)
{
	charNode* curNode = _listHead;
	int recNum = 1;

	while (curNode != nullptr) {
		if (pos == recNum) {
			return curNode->c;
		}
		curNode = curNode->next;
		recNum++;
	}

	return ' '; // ...or some other sentinel that makes sense.
}

void VLString::Concatenate(VLString &s2)
{
	charNode* curNode = _listHead;
	charNode* s2Node = s2._listHead;

	// Can't concatenate empty strings, so return.
	if (curNode == nullptr && s2Node == nullptr)
		return;

	// Cycle through to the last pointer in this list.
	if (curNode != nullptr) {
		while (curNode->next != nullptr) {
			curNode = curNode->next;
		}
	}

	// Copy over the nodes from s2 into this list.
	while (s2Node != nullptr) {
		if (curNode == nullptr) {
			_listHead = new charNode(*s2Node);
			_listHead->next = nullptr;
			curNode = _listHead;
		}
		else {
			charNode* newNode = new charNode(*s2Node);
			newNode->next = nullptr;
			curNode->next = newNode;
			curNode = curNode->next;
		}
		s2Node = s2Node->next;
	}
}

void VLString::Remove(int delOffset, int delLen)
{
	// Return if requested length to delete is zero or less
	if (delLen <= 0) {
		std::cerr << "Length of string to remove is equal to or less than zero." << std::endl;
		return;
	}

	// Return if this list is empty
	if (_listHead == nullptr) {
		return;
	}

	charNode* curNode = _listHead;
	charNode* prevNode = nullptr; // Node just behind curNode
	int numCurNode = 0;

	// Process if deleting from start of string
	if (delOffset == 0) {
		while (curNode->next != nullptr && numCurNode < delLen) {
			charNode* tmpNode = curNode;
			curNode = curNode->next;
			delete tmpNode;
			numCurNode++;
		}

		// Delete last node if requested
		if (numCurNode < delLen) {
			delete curNode;
			curNode = nullptr;
		}
		// Reset head of list
		_listHead = curNode;

		return;
	}


	// Get the node at position 'offset'
	while (curNode->next != nullptr && numCurNode < delOffset) {
		prevNode = curNode;
		curNode = curNode->next;
		numCurNode++;
	}

	// Return if offset is too high
	if (numCurNode < delOffset) {
		std::cerr << "Offset of string to remove is greater than length of string." << std::endl;
		return;
	}

	// Delete all nodes from the offset plus the length
	while (curNode->next != nullptr && numCurNode < delOffset + delLen) {
		prevNode->next = curNode->next;
		delete curNode;
		curNode = prevNode->next;
		numCurNode++;
	}

	// If the offset is zero and the length is longer than the string length, delete last item in list.
	if (numCurNode < delOffset + delLen) {
		prevNode->next = nullptr;
		delete curNode;
	}
}

void VLString::Print()
{
	charNode* curNode = _listHead;
	while (curNode != nullptr) {
		std::cout << curNode->c;
		curNode = curNode->next;
	}
	std::cout << std::endl;
}

void VLString::DeleteList(charList listPtr)
{
	charNode* curNode = _listHead;
	while (curNode != nullptr) {
		charNode *tmpNode = curNode;
		curNode = curNode->next;
		delete tmpNode;
	}
}

// Copy constructor
VLString::charList VLString::CopiedList(const charList original)
{
	// Return if no string to copy
	if (original == nullptr) {
		return nullptr;
	}

	// Create new list and copy first node from original list
	charList newCharList = new charNode;
	newCharList->c = original->c;

	// Get the next original list node
	charNode *originalNode = original->next;

	// Prepare a new node for this list
	charNode *newNode = newCharList;

	// For each original node, create a new node in this list and copy original node's data into it
	while (originalNode != nullptr) {
		newNode->next = new charNode;
		newNode = newNode->next;
		newNode->c = originalNode->c;
		originalNode = originalNode->next;
	}

	// Set last node's next to null
	newNode->next = nullptr;

	// Return the new list
	return newCharList;
}
