#include <iostream>
#include "VLString.h"

int main()
{
	VLString v1;
	v1.Append('a');
	v1.Append('b');
	v1.Append('c');

	VLString v2;
	v2.Append('d');
	v2.Append('e');

	std::cout << "Append" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;

	v1.Concatenate(v2);

	std::cout << "Concatenate" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;

	v1 = v2;

	std::cout << "Assignment" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;

	return 0;
}


