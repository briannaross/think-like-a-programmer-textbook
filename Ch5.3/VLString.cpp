#include <iostream>
#include "VLString.h"

VLString::VLString()
{
	_listHead = nullptr;
}

VLString::VLString(char c)
{
	//_listHead = new charNode;
	//_listHead->c = c;
	//_listHead->next = nullptr;
	Append(c);
}

VLString::~VLString()
{
	DeleteList(_listHead);
}

VLString& VLString::operator=(const VLString &rhs)
{
	if (this != &rhs) {
		DeleteList(_listHead);
		_listHead = CopiedList(rhs._listHead);
	}

	return *this;
}

void VLString::Append(char c)
{
	// Create the new node
	charNode* newNode = new charNode{ c };
	newNode->next = nullptr;

	// Set listhead to the new node if list is empty
	if (_listHead == nullptr) {
		_listHead = newNode;
		return;
	}

	// Create a copy of listhead to cycle through the list
	charNode* curNode = _listHead;

	// Cycle through the list to the end
	while (curNode->next != nullptr) {
		curNode = curNode->next;
	}

	// Set the last node to the new node
	curNode->next = newNode;
}

char VLString::CharacterAt(int pos)
{
	charNode* curNode = _listHead;
	int recNum = 1;

	while (curNode != nullptr) {
		if (pos == recNum) {
			return curNode->c;
		}
		curNode = curNode->next;
		recNum++;
	}

	return ' '; // ...or some other sentinel that makes sense.
}

void VLString::Concatenate(VLString &s2)
{
	charNode* curNode = _listHead;
	charNode* s2Node = s2._listHead;

	// Can't concatenate empty strings, so return.
	if (curNode == nullptr && s2Node == nullptr)
		return;

	// Cycle through to the last pointer in this list.
	if (curNode != nullptr) {
		while (curNode->next != nullptr) {
			curNode = curNode->next;
		}
	}

	// Copy over the nodes from s2 into this list.
	while (s2Node != nullptr) {
		if (curNode == nullptr) {
			_listHead = new charNode(*s2Node);
			_listHead->next = nullptr;
			curNode = _listHead;
		}
		else {
			charNode* newNode = new charNode(*s2Node);
			newNode->next = nullptr;
			curNode->next = newNode;
			curNode = curNode->next;
		}
		s2Node = s2Node->next;
	}
}

void VLString::Print()
{
	charNode* curNode = _listHead;
	while (curNode != nullptr) {
		std::cout << curNode->c;
		curNode = curNode->next;
	}
	std::cout << std::endl;
}

void VLString::DeleteList(charList listPtr)
{
	charNode* curNode = _listHead;
	while (curNode != nullptr) {
		charNode *tmpNode = curNode;
		curNode = curNode->next;
		delete tmpNode;
	}
}

VLString::charList VLString::CopiedList(const charList original)
{
	if (original == nullptr) {
		return nullptr;
	}

	charList newCharList = new charNode;
	newCharList->c = original->c;

	charNode *originalNode = original->next;
	charNode *newNode = newCharList;

	while (originalNode != nullptr) {
		newNode->next = new charNode;
		newNode = newNode->next;
		newNode->c = originalNode->c;
		originalNode = originalNode->next;
	}

	newNode->next = nullptr;

	return newCharList;
}
