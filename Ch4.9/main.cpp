#include <iostream>

struct Number {
	char digit;
	Number* next;
};

void IntToList(Number * &number);
void ReadNumber(Number * &number);
void PrintNumber(Number * &number);
void Cleanup(Number * &number);

int main()
{
	Number* number = nullptr;

	//ReadNumber(number);
	IntToList(number);
	PrintNumber(number);

	Cleanup(number);

	return 0;
}

void IntToList(Number * &number)
{
	Number* firstNum = nullptr;
	int integer = 0;;

	std::cout << "Enter an integer: ";
	std::cin >> integer;

	if (integer > 0) {
		number = new Number{ (integer % 10) + '0' };
		firstNum = number;
		number->next = nullptr;
		integer /= 10;

		while (integer > 0) {
			Number *curNumber = new Number{ (integer % 10) + '0' };
			number->next = curNumber;
			number = number->next;
			integer /= 10;
		}
	}

	number = firstNum;
}

void ReadNumber(Number * &number)
{
	Number *curNumber = nullptr;
	char digit = ' ';

	std::cout << "Enter a number: " << std::endl;
	digit = std::cin.get();

	if (digit != 10) {
		number = new Number{ digit };
		number->next = nullptr;
		curNumber = number;

		while (digit != 10) {
			digit = std::cin.get();
			Number* newNumber = new Number{ digit };
			newNumber->next = nullptr;
			curNumber->next = newNumber;
			curNumber = newNumber;
		}
	}
}

void PrintNumber(Number * &number)
{
	Number *curNumber = number;
	char digit = ' ';

	std::cout << "You entered the number: ";
	while (curNumber != nullptr) {
		std::cout << curNumber->digit - '0';
		curNumber = curNumber->next;
	}
}

void Cleanup(Number * &number)
{
	Number *curNumber = number;

	while (number != nullptr) {
		curNumber = number;
		number = number->next;
		delete curNumber;
	}

	number = nullptr;
	curNumber = nullptr;
}
