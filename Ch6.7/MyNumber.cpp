#include "MyNumber.h"



MyNumber::MyNumber()
{
	_listHead = nullptr;
}


MyNumber::~MyNumber()
{
	Number* pCurNum = _listHead;

	while (pCurNum != nullptr) {
		Number* pThisNum = pCurNum;
		pCurNum = pCurNum->next;
		delete pThisNum;
	}

	_listHead = nullptr;
}

void MyNumber::AddNumber(int n)
{
	Number* pCurNum = _listHead;

	if (pCurNum == nullptr) {
		Number* pNewNum = new Number;
		pNewNum->n = n;
		pNewNum->next = nullptr;
		_listHead = pNewNum;
		return;
	}

	while (pCurNum->next != nullptr) {
		pCurNum = pCurNum->next;
	}

	Number* pNewNum = new Number;
	pNewNum->n = n;
	pNewNum->next = nullptr;
	pCurNum->next = pNewNum;
}

int MyNumber::TargetSumIter(int TARGET)
{
	Number* pCurNum = _listHead;
	int sum = 0;

	while (pCurNum != nullptr) {
		if (pCurNum->n == TARGET) {
			sum++;
		}
		pCurNum = pCurNum->next;
	}

	return sum;
}

int MyNumber::TargetSumRecur(int TARGET)
{
	Number* pCurNum = _listHead;

	return PrivTargetSumRecur(pCurNum, TARGET);
}

int MyNumber::PrivTargetSumRecur(Number* pCurNum, int TARGET)
{
	if (pCurNum == nullptr) return false;

	int count = PrivTargetSumRecur(pCurNum->next, TARGET);

	if (pCurNum->n == TARGET)
		count++;

	return count;
}
