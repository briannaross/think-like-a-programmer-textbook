#include <iostream>
#include "MyNumber.h"

int main()
{
	const int TARGET = 5;

	MyNumber numbers;

	numbers.AddNumber(1);
	numbers.AddNumber(5);
	numbers.AddNumber(3);
	numbers.AddNumber(4);
	numbers.AddNumber(5);
	numbers.AddNumber(6);
	numbers.AddNumber(7);
	numbers.AddNumber(8);
	numbers.AddNumber(5);
	numbers.AddNumber(10);

	std::cout << "Iterative target sum: " << numbers.TargetSumIter(TARGET) << std::endl;

	std::cout << "Recursive target sum: " << numbers.TargetSumRecur(TARGET) << std::endl;

	return 0;
}
