#pragma once
class MyNumber
{
public:
	MyNumber();
	~MyNumber();
	void AddNumber(int n);
	int TargetSumIter(int TARGET);
	int TargetSumRecur(int TARGET);

private:
	struct Number {
		int n;
		Number* next;
	};

	Number* _listHead;

	int PrivTargetSumRecur(Number* pCurNum, int TARGET);
};

