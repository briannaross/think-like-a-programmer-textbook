#include <iostream>
#include <time.h>

int main()
{

	const int ARRAY_SIZE = 100;
	int array[ARRAY_SIZE];
	int modeFrequency[ARRAY_SIZE + 1];
	int mostCommonMode = 0;

	srand(time(NULL));


	for (int i = 0; i < ARRAY_SIZE; i++){
		array[i] = rand() % 100;
	}
	for (int i = 0; i <= ARRAY_SIZE; i++) {
		modeFrequency[i] = 0;
	}

	for (int i = 0; i < ARRAY_SIZE; i++) {
		modeFrequency[array[i]]++;
	}

	for (int i = 0; i <= ARRAY_SIZE; i++) {
		std::cout << i << ": " << modeFrequency[i] << std::endl;
		if (modeFrequency[i] > mostCommonMode) {
			mostCommonMode = i;
		}
	}

	std::cout << "Most common mode: " << modeFrequency[mostCommonMode] << " (" << mostCommonMode << ")" << std::endl;

	return 0;
}