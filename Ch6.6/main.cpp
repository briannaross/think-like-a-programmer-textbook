#include <iostream>
#include "MyNumber.h"

int main()
{
	MyNumber numbers;

	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(0);
	numbers.AddNumber(1);

	std::cout << "Iterative sum: " << numbers.SumIter() << std::endl;

	std::cout << "Recursive sum: " << numbers.SumRecur() << std::endl;

	return 0;
}
