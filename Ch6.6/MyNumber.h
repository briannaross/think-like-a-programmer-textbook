#pragma once
class MyNumber
{
public:
	MyNumber();
	~MyNumber();
	void AddNumber(int n);
	int SumIter();
	bool SumRecur();

private:
	struct Number {
		bool n;
		Number* next;
	};

	Number* _listHead;

	bool PrivSumRecur(Number* pCurNum);
};

