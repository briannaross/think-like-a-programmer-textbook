#include "MyNumber.h"



MyNumber::MyNumber()
{
	_listHead = nullptr;
}


MyNumber::~MyNumber()
{
	Number* pCurNum = _listHead;

	while (pCurNum != nullptr) {
		Number* pThisNum = pCurNum;
		pCurNum = pCurNum->next;
		delete pThisNum;
	}

	_listHead = nullptr;
}

void MyNumber::AddNumber(int n)
{
	Number* pCurNum = _listHead;

	if (pCurNum == nullptr) {
		Number* pNewNum = new Number;
		pNewNum->n = n;
		pNewNum->next = nullptr;
		_listHead = pNewNum;
		return;
	}

	while (pCurNum->next != nullptr) {
		pCurNum = pCurNum->next;
	}

	Number* pNewNum = new Number;
	pNewNum->n = n;
	pNewNum->next = nullptr;
	pCurNum->next = pNewNum;
}

int MyNumber::SumIter()
{
	Number* pCurNum = _listHead;
	int sum = 0;

	while (pCurNum != nullptr) {
		if (pCurNum->n == 1) {
			sum++;
		}
		pCurNum = pCurNum->next;
	}

	return sum % 2;
}

bool MyNumber::SumRecur()
{
	Number* pCurNum = _listHead;

	return PrivSumRecur(pCurNum);
}

bool MyNumber::PrivSumRecur(Number* pCurNum)
{
	if (pCurNum == nullptr) return false;

	bool nextVal = PrivSumRecur(pCurNum->next);

	if (pCurNum->n == nextVal)
		return false;
	else
		return true;
}
