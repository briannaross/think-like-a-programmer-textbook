#include <iostream>
#include <string>

struct Student {
	int grade;
	int studentID;
	std::string name;
	Student* next;
};

typedef Student* StudentCollection;

enum {
	GRADE_ONE = 0,
	GRADE_TWO = 1,
	GRADE_THREE = 2,
} grades;


int main()
{

	const int ARRAY_SIZE = 10;
	StudentCollection sc;

	Student* s1 = new Student{ 87, 10001, "Fred" };
	Student* s2 = new Student{ 28, 10002, "Tom" };
	Student* s3 = new Student{ 100, 10003, "Alistair" };
	Student* s4 = new Student{ 78, 10004, "Sasha" };
	Student* s5 = new Student{ 84, 10005, "Erin" };
	Student* s6 = new Student{ 98, 10006, "Belinda" };
	Student* s7 = new Student{ 75, 10007, "Leslie" };
	Student* s8 = new Student{ 70, 10008, "Candy" };
	Student* s9 = new Student{ 81, 10009, "Aretha" };
	Student* s10 = new Student{ 68, 10010, "Veronica" };

	s1->next = s2;
	s2->next = s3;
	s3->next = s4;
	s4->next = s5;
	s5->next = s6;
	s6->next = s7;
	s7->next = s8;
	s8->next = s9;
	s9->next = s10;
	s10->next = nullptr;

	sc = s1;

	int gradeQuartiles[4] = { 0, 0, 0, 0 };

	Student* studentRec = sc;
	while (sc != nullptr) {
		if (sc->grade >= 75) {
			gradeQuartiles[GRADE_THREE]++;
		}
		else if (sc->grade >= 50) {
			gradeQuartiles[GRADE_TWO]++;
		}
		else if (sc->grade >= 25) {
			gradeQuartiles[GRADE_ONE]++;
		}
		sc = sc->next;
	}

	std::cout << "Grade quartiles" << std::endl;
	for (size_t i = GRADE_ONE; i <= GRADE_THREE; i++) {
		std::cout << "Grade " << i + 1 << ": " << gradeQuartiles[i] << std::endl;
	}

	return 0;
}
