#include <iostream>

int NumOccurIter(const int array[], const int SIZE, const int TARGET);
int NumOccurRecur(const int array[], const int SIZE, const int offset, const int TARGET);

int main()
{
	const int SIZE = 10;
	const int TARGET = 5;
	const int array[SIZE] = { 8, 3, 5, 6, 5, 7, 4, 1, 2, 5 };
	const int offset = 0;

	std::cout << "Iterative instances of " << TARGET << ": " << NumOccurIter(array, SIZE, TARGET) << std::endl;
	std::cout << "Recursive instances of " << TARGET << ": " << NumOccurRecur(array, SIZE, offset, TARGET) << std::endl;

	return 0;
}

int NumOccurIter(const int array[], const int SIZE, const int TARGET)
{
	int sumOccurences = 0;

	for (int i = 0; i < SIZE; i++) {
		if (array[i] == 5) {
			sumOccurences++;
		}
	}

	return sumOccurences;
}

int NumOccurRecur(const int array[], const int SIZE, const int offset, const int TARGET)
{
	if (SIZE == offset) return 0;

	int sumOccurences =+ NumOccurRecur(array, SIZE, offset + 1, TARGET);

	if (array[offset] == TARGET)
		sumOccurences++;

	return sumOccurences;
}
