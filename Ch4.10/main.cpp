#include <iostream>

struct Number {
	char digit;
	Number* next;
};

void IntToList(Number * &number, int i);
void ReadNumber(Number * &number);
Number* AddNumbers(Number * &number1, Number * &number2);
void PrintNumber(Number * &number);
void Cleanup(Number * &number);

int main()
{
	Number* number1 = nullptr;
	Number* number2 = nullptr;
	Number* number3 = nullptr;

	//ReadNumber(number);
	IntToList(number1, 123);
	IntToList(number2, 456);

	number3 = AddNumbers(number1, number2);

	PrintNumber(number3);

	Cleanup(number1);
	Cleanup(number2);
	Cleanup(number3);

	return 0;
}

void IntToList(Number * &number, int i)
{
	Number* firstNum = nullptr;
	int integer = i;

	std::cout << "Enter an integer: ";
	std::cin >> integer;
	std::cin.ignore(INT_MAX, 10);
	std::cin.clear();

	if (integer > 0) {
		number = new Number{ (integer % 10) + '0' };
		firstNum = number;
		number->next = nullptr;
		integer /= 10;

		while (integer > 0) {
			Number *curNumber = new Number{ (integer % 10) + '0' };
			number->next = curNumber;
			number = number->next;
			integer /= 10;
		}
	}

	number = firstNum;
}

void ReadNumber(Number * &number)
{
	Number *curNumber = nullptr;
	char digit = ' ';

	std::cout << "Enter a number: " << std::endl;
	digit = std::cin.get();

	if (digit != 10) {
		number = new Number{ digit };
		number->next = nullptr;
		curNumber = number;

		while (digit != 10) {
			digit = std::cin.get();
			Number* newNumber = new Number{ digit };
			newNumber->next = nullptr;
			curNumber->next = newNumber;
			curNumber = newNumber;
		}
	}
}

Number* AddNumbers(Number * &number1, Number * &number2)
{
	Number* result = nullptr; new Number{ ' ' };
	Number* curResult = nullptr;
	Number* num1 = number1;
	Number* num2 = number2;
	int remainder = 0;
	int digitSum = 0;


	while (num1 != nullptr || num2 != nullptr) {
		if (num1 == nullptr) {
			// insert number1 added with any remainder into result list
			digitSum = (num2->digit - '0') + remainder;
			num2 = num2->next;
		}
		else if (num2 == nullptr) {
			// insert number2 added with any remainder into result list
			digitSum = (num1->digit - '0') + remainder;
			num1 = num1->next;
		}
		else {
			// insert sum of current digits from number1 and number2, added with any remainder, into result list
			digitSum = (num1->digit - '0') + (num2->digit - '0') + remainder;
			num1 = num1->next;
			num2 = num2->next;
		}

		if (digitSum >= 10)
			remainder = 1;
		else
			remainder = 0;

		if (result == nullptr) {
			result = new Number{ (digitSum % 10) + '0' };
			curResult = result;
		}
		else {
			Number* curNumber = new Number{ (digitSum % 10) + '0' };
			curNumber->next = nullptr;
			curResult->next = curNumber;
			curResult = curResult->next;
		}
	}

	if (remainder > 0) {
		Number* curNumber = new Number{ (remainder % 10) + '0' };
		curNumber->next = nullptr;
		curResult->next = curNumber;
	}

	return result;
}

void PrintNumber(Number * &number)
{
	Number *curNumber = number;
	char digit = ' ';
	int result = 0;
	int counter = 1;

	while (curNumber != nullptr) {
		result += ((curNumber->digit - '0') * counter);
		counter *= 10;
		curNumber = curNumber->next;
	}

	std::cout << "The number is: " << result << std::endl;
}

void Cleanup(Number * &number)
{
	Number *curNumber = number;

	while (number != nullptr) {
		curNumber = number;
		number = number->next;
		delete curNumber;
	}

	number = nullptr;
	curNumber = nullptr;
}
