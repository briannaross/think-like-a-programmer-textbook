#include <iostream>

bool IsArraySorted(void* array, int ARRAY_SIZE);

int main()
{
	const int NUM_MONTHS = 12;
	//int sales[NUM_MONTHS] = { 1856, 498, 30924, 87478, 328, 2653, 387, 3754, 387587, 2873, 276, 32 };
	int *sales = new int[NUM_MONTHS];
	sales[0] = 32;
	sales[1] = 276;
	sales[2] = 328;
	sales[3] = 387;
	sales[4] = 498;
	sales[5] = 1856;
	sales[6] = 2653;
	sales[7] = 2873;
	sales[8] = 3754;
	sales[9] = 30924;
	sales[10] = 87478;
	sales[11] = 387587;

	if (IsArraySorted((void*)sales, NUM_MONTHS)) {
		std::cout << "Array is sorted" << std::endl;
	}
	else {
		std::cout << "Array is not sorted" << std::endl;
	}

	return 0;
}

// Might be better to use templates these days instead of voids.
bool IsArraySorted(void* array, int ARRAY_SIZE)
{
	int* a = (int *)array;

	for (size_t i = 0; i < ARRAY_SIZE - 1; i++) {
		if (a[i] > a[i + 1]) {
			return false;
		}
	}

	return true;
}
