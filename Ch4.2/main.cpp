#include <iostream>

typedef char* ArrayString;

int length(ArrayString &s);
void append(ArrayString &s, char c);
void concatenate(ArrayString &s1, ArrayString s2);
ArrayString substring(ArrayString &s, int startPos, int length);

int main()
{
	ArrayString a = new char[5];
	a[0] = 't'; a[1] = 'e'; a[2] = 's'; a[3] = 't'; a[4] = 0;
	ArrayString c = new char[1];
	c[0] = 0;

	concatenate(c, a);

	a = substring(a, 0, 800);
	std::cout << a << "\n" << c << "\n";
	std::cout << (void*)a << "\n" << (void*)c << "\n";

	delete[] a;
	delete[] c;

	return 0;
}

int length(ArrayString &s)
{
	int count = 0;

	while (s[count] != 0) {
		count++;
	}

	return count;
}

void append(ArrayString &s, char c)
{
	int oldLength = length(s);
	ArrayString newStr = new char[oldLength + 1];

	for (int i = 0; i < oldLength; i++) {
		newStr[i] = s[i];
	}
	newStr[oldLength] = c;
	newStr[oldLength + 1] = 0;
	delete[] s;
	s = newStr;
}

void concatenate(ArrayString &s1, ArrayString s2)
{
	int s1_OldLength = length(s1);
	int s2_Length = length(s2);
	int s1_NewLength = s1_OldLength + s2_Length;
	ArrayString newStr = new char[s1_NewLength + 1];

	for (int i = 0; i < s1_OldLength; i++) {
		newStr[i] = s1[i];
	}
	for (int i = 0; i < s2_Length; i++) {
		newStr[s1_OldLength + i] = s2[i];
	}
	newStr[s1_NewLength] = 0;
	delete[] s1;
	s1 = newStr;
}

ArrayString substring(ArrayString &s, int startPos, int substrLen)
{

	int strLen = length(s);
	int newStrLen;

	// Set the length of the new string to the requested substring length, or if the requested
	// substring length goes beyond the end of the string, set the length of the new string to
	// the start position plus whatever number of characters remain.
	if (strLen > startPos + substrLen)
		newStrLen = substrLen;
	else
		newStrLen = strLen;

	// Copy the substring into the new array
	ArrayString newStr = new char[newStrLen + 1];
	for (int i = startPos; i <= newStrLen; i++) {
		newStr[i - startPos] = s[i];
	}
	newStr[newStrLen] = 0;

	return newStr;
}