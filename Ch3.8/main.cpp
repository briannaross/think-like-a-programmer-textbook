#include <iostream>

struct student {
	int grade;
	int studentID;
	std::string name;
};

enum {
	GRADE_ONE = 0,
	GRADE_TWO = 1,
	GRADE_THREE = 2,
} grades;


int main()
{

	const int ARRAY_SIZE = 10;
	student studentArray[ARRAY_SIZE] = {
		{ 87, 10001, "Fred" },
		{ 28, 10002, "Tom" },
		{ 100, 10003, "Alistair" },
		{ 78, 10004, "Sasha" },
		{ 84, 10005, "Erin" },
		{ 98, 10006, "Belinda" },
		{ 75, 10007, "Leslie" },
		{ 70, 10008, "Candy" },
		{ 81, 10009, "Aretha" },
		{ 68, 10010, "Veronica" }
	};
	int gradeQuartiles[4] = { 0, 0, 0, 0 };

	for (size_t i = 0; i < ARRAY_SIZE; i++) {
		if (studentArray[i].grade >= 75) {
			gradeQuartiles[GRADE_THREE]++;
		}
		else if (studentArray[i].grade >= 50) {
			gradeQuartiles[GRADE_TWO]++;
		}
		else if (studentArray[i].grade >= 25) {
			gradeQuartiles[GRADE_ONE]++;
		}
	}

	std::cout << "Grade quartiles" << std::endl;
	for (size_t i = GRADE_ONE; i <= GRADE_THREE; i++) {
		std::cout << "Grade " << i + 1 << ": " << gradeQuartiles[i] << std::endl;
	}

	return 0;
}
