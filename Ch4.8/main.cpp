#include <iostream>

struct Character {
	char c;
	Character* next;
};

typedef Character* LLString;

void Append(LLString &str, char c);
char CharacterAt(LLString &str, int pos);
void Concatenate(LLString &s1, LLString &s2);
void RemoveChars(LLString &str, int offset, int len);

int main()
{
	// Initialisation
	LLString str;
	Character* c1 = new Character{ 'a' };
	Character* c2 = new Character{ 'b' };
	Character* c3 = new Character{ 'c' };
	c1->next = c2;
	c2->next = c3;
	c3->next = nullptr;
	str = c1;

	LLString str2;
	Character* c4 = new Character{ 'd' };
	Character* c5 = new Character{ 'e' };
	Character* c6 = new Character{ 'f' };
	c4->next = c5;
	c5->next = c6;
	c6->next = nullptr;
	str2 = c4;

	// Do some LLString manipulations
	Append(str, 'd');

	int pos = 4;
	std::cout << "Character at position " << pos << " is " << CharacterAt(str, pos) << std::endl;

	// Concatenate strings, then output str
	Concatenate(str, str2);

	Character* curRec = str;
	while (curRec != nullptr) {
		std::cout << curRec->c;
		curRec = curRec->next;
	}
	std::cout << std::endl;

	// Remove characters from str, then output str
	//RemoveChars(str, 0, 0);
	//RemoveChars(str, 1, 0);
	//RemoveChars(str, 0, 1);
	//RemoveChars(str, 1, 1);
	RemoveChars(str, 4, 2);
	//RemoveChars(str, 0, 8);
	//RemoveChars(str, 8, 0);

	curRec = str;
	while (curRec != nullptr) {
		std::cout << curRec->c;
		curRec = curRec->next;
	}
	std::cout << std::endl;


	// Cleanup
	curRec = str;
	while (curRec != nullptr) {
		Character* thisRec = curRec;
		curRec = curRec->next;
		delete thisRec;
	}
	curRec = str2;
	while (curRec != nullptr) {
		Character* thisRec = curRec;
		curRec = curRec->next;
		delete thisRec;
	}

	curRec = nullptr;
	str = nullptr;
	str2 = nullptr;
	c1 = nullptr;
	c2 = nullptr;
	c3 = nullptr;
	c4 = nullptr;
	c5 = nullptr;
	c6 = nullptr;

	return 0;
}


void Append(LLString &str, char c)
{
	if (str == nullptr) {
		return;
	}

	Character* curRec = str;
	Character* newRec = new Character{ c };

	while (curRec->next != nullptr) {
		curRec = curRec->next;
	}

	newRec->next = nullptr;
	curRec->next = newRec;
}

char CharacterAt(LLString &str, int pos)
{
	if (str == nullptr) {
		return ' ';
	}

	Character* curRec = str;
	int recNum = 1;

	while (curRec != nullptr) {
		if (pos == recNum) {
			return curRec->c;
		}
		curRec = curRec->next;
		recNum++;
	}

	return ' ';
}

void Concatenate(LLString &s1, LLString &s2)
{
	if (s1 == nullptr && s2 == nullptr) {
		return;
	}

	Character* curRecS1 = s1;
	Character* curRecS2 = s2;

	while (curRecS1->next != nullptr) {
		curRecS1 = curRecS1->next;
	}

	while (curRecS2 != nullptr) {
		Character* newRec = new Character(*curRecS2);
		newRec->next = nullptr;

		curRecS1->next = newRec;
		curRecS1 = newRec;

		curRecS2 = curRecS2->next;
	}
}

void RemoveChars(LLString &str, int offset, int len)
{
	if (str == nullptr) {
		return;
	}

	Character* curRec = str;
	Character* recAtOffset = str;
	int recNum = 0;

	offset = (offset > 0) ? offset - 1 : 0;

	if (offset == 0) {
		while (curRec->next != nullptr && recNum < len) {
			Character* thisRec = curRec;
			curRec = curRec->next;
			delete thisRec;
			recNum++;
		}

		if (recNum < len) {
			delete curRec;
			curRec = nullptr;
		}
		str = curRec;
		return;
	}

	
	// Get the node at position 'offset'
	while (curRec->next != nullptr && recNum < offset) {
		recAtOffset = curRec;
		curRec = curRec->next;
		recNum++;
	}

	if (recNum < offset) {
		return;
	}

	// Delete all nodes from the offset plus the length
	while (curRec->next != nullptr && recNum < offset + len) {
		recAtOffset->next = curRec->next;
		delete curRec;
		curRec = recAtOffset->next;
		recNum++;
	}

	// If the offset is zero and the length is longer than the string length
	if (recNum < offset + len) {
		recAtOffset->next = nullptr;
		delete curRec;
	}
}
