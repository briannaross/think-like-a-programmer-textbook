#include <iostream>
#include <string>

struct student {
	int grade;
	int studentID;
	std::string name;
};

int CompareFunc(const void* voidA, const void* voidB);

int main()
{
	const int ARRAY_SIZE = 10;
	student studentArray[ARRAY_SIZE] = {
		{ 87, 10001, "Fred" },
		{ 28, 10002, "Tom" },
		{ 100, 10003, "Alistair" },
		{ 78, 10004, "Sasha" },
		{ 84, 10005, "Erin" },
		{ 98, 10006, "Belinda" },
		{ 75, 10007, "Leslie" },
		{ 70, 10008, "Candy" },
		{ 81, 10009, "Aretha" },
		{ 68, 10010, "Veronica" }
	};

	qsort(studentArray, ARRAY_SIZE, sizeof(student), CompareFunc);

	for (size_t i = 0; i < ARRAY_SIZE; i++) {
		std::cout << studentArray[i].name << " (" << studentArray[i].studentID << "): " << studentArray[i].grade << std::endl;
	}

	return 0;
}

// Always send in pointers to the compare function to be used for qsort as void.
// From microsoft.com: If a pointer's type is void, the pointer can point to any variable that is not declared with the **const* or volatile keyword.
// A void pointer cannot be dereferenced unless it is cast to another type. A void pointer can be converted into any other type of data pointer. 
int CompareFunc(const void* voidA, const void* voidB)
{
	// Cast pointers back to whatever I want, be it a scalar or non-scalar
	student* s1 = (student*)(voidA);
	student* s2 = (student*)(voidB);

	return s1->studentID - s2->studentID;
}