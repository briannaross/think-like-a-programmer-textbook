#pragma once
#include <vector>
#include <map>

class BTree
{
private:
	struct Node {
		int data;
		Node* left;
		Node* right;
	};

public:
	BTree();
	~BTree();
	bool Find(int n);
	void Insert(int n);
	bool Delete(int n);
	bool IsBST();
	float Average();
	int Mode();
	int Median();
	void PrintTree();

private:
	Node* root;
	int numNodes;
	Node* FindNode(Node* curNode, Node *prevNode, int n);
	void InsertNode(Node* curNode, Node* newNode);
	void DeleteNode(Node* curNode);
	bool CheckIfBST(Node*& curNode);
	int AddNodeValues(Node* curNode);
	void PushNodeValues(Node* curNode, std::vector<int> &vec);
	void MapNodeValues(Node* curNode, std::map<int, int> &nodemap);

	void PrintNode(Node*& curNode);
};

