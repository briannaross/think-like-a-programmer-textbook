#include <iostream>
#include "BTree.h"

int main()
{
	BTree bt;

	bt.Insert(8);
	bt.Insert(3);
	bt.Insert(7);
	bt.Insert(4);
	bt.Insert(2);
	bt.Insert(6);
	bt.Insert(2);
	bt.Insert(9);
	bt.Insert(2);
	bt.Insert(0);
	bt.Insert(51069);
	bt.Insert(51070);
	bt.Insert(51069);

	bt.PrintTree();

	std::cout << "Average value: " << bt.Average() << std::endl;
	std::cout << "Median value: " << bt.Median() << std::endl;
	std::cout << "Mode value: " << bt.Mode() << std::endl;

	return 0;
}