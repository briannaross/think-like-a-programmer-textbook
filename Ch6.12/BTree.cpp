#include "BTree.h"
#include <iostream>
#include <algorithm>

bool CompareInts(int i, int j) { return (i < j); }

BTree::BTree()
{
	root = nullptr;
	numNodes = 0;
}

BTree::~BTree()
{
	Node* curNode = root;
	DeleteNode(curNode);
}

bool BTree::Find(int n)
{
	// Nothing in tree so return
	if (root == nullptr) return false;

	Node* curNode = root;
	Node* prevNode = nullptr;
	prevNode = FindNode(curNode, prevNode, n);

	// No matching node found so return
	if (prevNode == nullptr) return false;

	return true;

	/*
	This code is redundant while were not passing a pointer back to any matching data.

	// FindNode returns the previous node, so we need to check which
	// leaf contains the matching data.
	if (prevNode->left->data == n) {
		curNode = prevNode->left;
	}
	else {
		curNode = prevNode->right;
	}

	if (curNode == nullptr) {
		return false;
	}
	else {
		return true;
	}
	*/
}

void BTree::Insert(int n)
{
	Node* newNode = new Node;
	newNode->data = n;
	newNode->left = nullptr;
	newNode->right = nullptr;
	numNodes++;

	Node* curNode = root;
	InsertNode(curNode, newNode);
}

bool BTree::Delete(int n)
{
	if (root == nullptr) return false;

	// Root node's data matches. Needs to be handled differently
	// to leaf nodes;
	if (root->data == n) {
		Node* tmpRoot = root;

		// If the left leaf node contains something then set it to root.
		if (root->left != nullptr) {
			Node* right = root->right;
			root = root->left;

			// Only find a new place for right if it contains something.
			if (right != nullptr) {
				InsertNode(root, right);
			}
		}
		// Straight forward replacement of root with right leaf node.
		else if (root->right != nullptr) {
			root = root->right;
		}
		// If nodes are null then we're deleing root, so the root needs to be
		// set to nullptr;
		else {
			root = nullptr;
		}
		delete tmpRoot;
		numNodes--;

		return true;
	}

	// When searching retain the previous node as well as the current node,
	// because we need to set the parent's corresponding leaf node to one
	// of the current node's leaf nodes.
	Node* curNode = root;
	Node* prevNode = nullptr;
	prevNode = FindNode(curNode, prevNode, n);

	// Nothing found
	if (prevNode == nullptr) return false;

	Node* matchingNode = nullptr;

	// If the matching data is found on the left leaf node move it's left leaf into prevNode->left.
	if (prevNode->left != nullptr && prevNode->left->data == n) {
		matchingNode = prevNode->left;
		Node* left = matchingNode->left;
		Node* right = matchingNode->right;
		prevNode->left = left;

		// Find a new home for the right leaf node.
		InsertNode(curNode, right);
	}
	// Left node is nullptr so we only need to move the right leaf node up one.
	else if (prevNode->right != nullptr) {
		matchingNode = prevNode->right;
		Node* right = matchingNode->right;
		prevNode->right = right;
	}

	// All moves are done so delete the matching node.
	delete matchingNode;
	numNodes--;

	return true;
}

bool BTree::IsBST()
{
	Node* curNode = root;
	Node* prevNode = nullptr;

	return CheckIfBST(curNode);
}

void BTree::PrintTree()
{
	//Node* curNode = root;

	if (root == nullptr) {
		std::cout << "No data in tree.";
	}
	else {
		PrintNode(root);
	}

	std::cout << std::endl;
}

float BTree::Average()
{
	Node* curNode = root;

	if (numNodes > 0) {
		return (float)AddNodeValues(curNode) / (float)numNodes;
	}
	else {
		return INT_MIN;
	}
}

int BTree::Mode()
{
	Node* curNode = root;
	std::map<int, int> nodemap;

	MapNodeValues(curNode, nodemap);

	int mode = INT_MIN;
	std::map<int, int>::iterator it = nodemap.begin();

	while ( it != nodemap.end()) {
		if (it->second > mode) {
			mode = it->first;
		}
		it++;
	}

	return mode;
}

int BTree::Median()
{
	Node* curNode = root;
	std::vector<int> vec;

	PushNodeValues(curNode, vec);

	if (vec.size() > 0) {
		std::sort(vec.begin(), vec.end(), CompareInts);
		return vec[vec.size() / 2];
	}
	else {
		return INT_MIN;
	}
	
}

BTree::Node* BTree::FindNode(Node* curNode, Node *prevNode, int n)
{
	// Dead end, so return nothing.
	if (curNode == nullptr) return nullptr;
	
	// A match, so return the previous leaf node. We do this so a delete
	// operation can delete curNode and insert it's left leaf into prevNode->left,
	// then find a new insertion point for curNode->right.
	if (curNode->data == n) return prevNode;

	// No match and no dead end, so keep searching.
	Node *left = FindNode(curNode->left, curNode, n);
	Node *right = FindNode(curNode->right, curNode, n);

	// This method only allows for one match. Everything will eventually return
	// nullptr unless one matching node is found. So we simply return anything
	// we find that's not null, because it's a match.
	if (left != nullptr) return left;
	if (right != nullptr) return right;

	return nullptr;
}

void BTree::InsertNode(Node* curNode, Node* newNode)
{
	if (newNode == nullptr) return;

	// New node will be the root if no root exists.
	if (curNode == nullptr) {
		curNode = newNode;
		if (root == nullptr) {
			root = newNode;
		}
		return;
	}

	// Insert node in curNode's left leaf if the data
	// is less than curNode's data, else insert it into
	// curNode's right leaf.
	if (newNode->data < curNode->data) {
		// Insert if empty
		if (curNode->left == nullptr) {
			curNode->left = newNode;
			return;
		}
		// Otherwise traverse further down the left
		else {
			InsertNode(curNode->left, newNode);
		}
	}
	else {
		// Insert if empty
		if (curNode->right == nullptr) {
			curNode->right = newNode;
			return;
		}
		// Otherwise traverse further down the right
		else {
			InsertNode(curNode->right, newNode);
		}
	}
}

void BTree::DeleteNode(Node* curNode)
{
	if (curNode == nullptr) return;

	DeleteNode(curNode->left);
	DeleteNode(curNode->right);

	// Only delete when we hit the last leaf of a branch.
	delete curNode;
	numNodes--;
}

bool BTree::CheckIfBST(Node*& curNode)
{
	if (curNode == nullptr) return true;

	bool isHeap = true;

	if (curNode->left != nullptr && curNode->left->data >= curNode->data)
		isHeap = false;
			
	if (curNode->right != nullptr && curNode->right->data < curNode->data)
		isHeap = false;

	if (isHeap == true)
		isHeap = CheckIfBST(curNode->left);

	if (isHeap == true)
		isHeap = CheckIfBST(curNode->right);

	return isHeap;
}

int BTree::AddNodeValues(Node* curNode)
{
	if (curNode == nullptr) return 0;

	return curNode->data + AddNodeValues(curNode->left) + AddNodeValues(curNode->right);
}

void BTree::PushNodeValues(Node* curNode, std::vector<int> &vec)
{
	if (curNode == nullptr) return;

	vec.push_back(curNode->data);

	PushNodeValues(curNode->left, vec);
	PushNodeValues(curNode->right, vec);
}

void BTree::MapNodeValues(Node* curNode, std::map<int, int> &nodemap)
{
	if (curNode == nullptr) return;

	if (!nodemap[curNode->data]) {
		nodemap[curNode->data] = 0;
	}

	nodemap[curNode->data]++;

	MapNodeValues(curNode->left, nodemap);
	MapNodeValues(curNode->right, nodemap);
}

void BTree::PrintNode(Node*& curNode)
{
	if (curNode == nullptr) return;

	// Print nodes from root, down the left then across.
	std::cout << curNode->data << ", ";

	PrintNode(curNode->left);
	PrintNode(curNode->right);
}
