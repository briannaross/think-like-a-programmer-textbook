#include <iostream>

int ConvertToDecimal(char* fromFormat);
void ConvertFromDecimal(char* toFormat, int num);

void FindDivisor(int &divisor, int num, int base);

int main()
{
	int num = 0;
	char digit = ' ';
	char menuOptionFrom = ' ';
	char menuOptionTo = ' ';

	while (menuOptionFrom != 'b' && menuOptionFrom != 'd' && menuOptionFrom != 'h') {
		std::cout << "What number format do you wish to convert FROM? (b)inary, (d)ecimal, or (h)exadecimal: ";
		menuOptionFrom = std::cin.get();
		std::cin.ignore(1);
		std::cin.clear();
	}

	while (menuOptionTo != 'b' && menuOptionTo != 'd' && menuOptionTo != 'h') {
		std::cout << "What number format do you wish to convert TO? (b)inary, (d)ecimal, or (h)exadecimal: ";
		menuOptionTo = std::cin.get();
		std::cin.ignore(1);
		std::cin.clear();
	}

	num = ConvertToDecimal(&menuOptionFrom);
	ConvertFromDecimal(&menuOptionTo, num);

	std::cout << std::endl;

	return 0;
}

int ConvertToDecimal(char* fromFormat)
{
	char digit = ' ';
	int sum = 0;

	std::cout << "Enter a number: ";
	digit = std::cin.get();

	if (*fromFormat == 'b') {
		while (digit != 10) {
			sum *= 2;
			sum += (digit - '0');

			digit = std::cin.get();
		}
	}
	else if (*fromFormat == 'h') {
		while (digit != 10) {
			sum *= 16;
			if (digit >= 65 && digit <= 70) {
				switch (digit) {
				case 65: sum += 10; break;
				case 66: sum += 11; break;
				case 67: sum += 12; break;
				case 68: sum += 13; break;
				case 69: sum += 14; break;
				case 70: sum += 15; break;
				}
			}
			else {
				sum += (digit - '0');
			}

			digit = std::cin.get();
		}
	}
	else {
		while (digit != 10) {
			sum *= 10;
			sum += (digit - '0');
			digit = std::cin.get();
		}
	}

	return sum;
}

void ConvertFromDecimal(char* toFormat, int num)
{
	int divisor = 1;

	if (*toFormat == 'b') {
		int base = 2;
		FindDivisor(divisor, num, base);

		std::cout << "Binary conversion: ";

		while (num > 0 || divisor > 0) {
			if (num >= divisor) {
				std::cout << num / divisor;
				num %= divisor;
			}
			else
				std::cout << 0;

			divisor /= base;
		}
	} else if (*toFormat == 'd') {
		int base = 10;

		FindDivisor(divisor, num, base);

		std::cout << "Decimal conversion: ";

		while (num > 0 || divisor > 0) {
			if (num >= divisor) {
				std::cout << num / divisor;
				num %= divisor;
			}
			else
				std::cout << 0;

			divisor /= base;
		}
	}
	else {
		int base = 16;

		FindDivisor(divisor, num, base);

		std::cout << "Hexadecimal conversion: ";

		while (num > 0 || divisor > 0) {
			if (num >= divisor) {
				char hexnum = ' ';
				switch (num / divisor) {
				case 10: hexnum = 'A'; break;
				case 11: hexnum = 'B'; break;
				case 12: hexnum = 'C'; break;
				case 13: hexnum = 'D'; break;
				case 14: hexnum = 'E'; break;
				case 15: hexnum = 'F'; break;
				}

				if (hexnum != ' ')
					std::cout << hexnum;
				else
					std::cout << num / divisor;

				num %= divisor;
			}
			else
				std::cout << 0;

			divisor /= base;
		}
	}
}

void FindDivisor(int &divisor, int num, int base)
{

	while (divisor < num) {
		divisor *= base;
	}

	if (divisor > num) {
		divisor /= base;
	}
}
