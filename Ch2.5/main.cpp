#include <iostream>

int TripleDigitValue(int digit);

int main()
{
	char digit = ' ';
	int sumOfDigits = 0;
	int checkSum = 0;
	int lastDigit = 0;
	int evenLengthChecksum = 0;
	int position = 1;

	std::cout << "Enter a number: ";
	digit = std::cin.get();

	while (digit != 10) {
		if (position == 13) {
			lastDigit = digit - '0';
			std::cout << lastDigit << std::endl;
		} 
		else {
			if (position % 2 == 0)
				sumOfDigits += TripleDigitValue(digit - '0');
			else
				sumOfDigits += digit - '0';
		}

		digit = std::cin.get();
		position++;
	}


	if (position < 13 || position > 14)
		std::cout << "Number is an invalid length.\n";
	else {
		checkSum = 10 - sumOfDigits % 10;

		std::cout << "Checksum: " << checkSum << ".\n";

		if (position == 14) {
			std::cout << "Number is 13 digits long.\n";
			if (checkSum == lastDigit)
				std::cout << "Last digit is equal to checksum. Valid.\n";
			else
				std::cout << "Last digit is not equal to checksum. Invalid.\n";
		}
		else {
			std::cout << "Number is 12 digits long.\n";
			std::cout << "Checksum is " << checkSum << "\n";
		}
	}

	std::cin.get();

	return 0;
}

int TripleDigitValue(int digit)
{
	return digit * 3;
}