#include <iostream>

typedef char* ArrayString;

int Length(ArrayString &s);
void Append(ArrayString &s, char c);
void Concatenate(ArrayString &s1, ArrayString s2);
ArrayString Substring(ArrayString &s, int startPos, int length);
void ReplaceString(ArrayString &sourceString, ArrayString &targetString, ArrayString &replaceText);
bool Compare(ArrayString &sourceStr, ArrayString &targetStr, int offset, int replaceTextLen);
void Replace(ArrayString &sourceStr, ArrayString &replaceText, int offset, int sourceStrLen, int targetStrLen, int replaceTextLen);

int main()
{
	//ArrayString a = new char[5];
	//a[0] = 't'; a[1] = 'e'; a[2] = 's'; a[3] = 't'; a[4] = 0;
	//ArrayString c = new char[1];
	//c[0] = 0;

	//Concatenate(c, a);

	//a = Substring(a, 0, 800);
	//std::cout << a << "\n" << c << "\n";
	//std::cout << (void*)a << "\n" << (void*)c << "\n";

	//delete[] a;
	//delete[] c;

	ArrayString x = new char[9];
	x[0] = 'a'; x[1] = 'b'; x[2] = 'c'; x[3] = 'd'; x[4] = 'a'; x[5] = 'b'; x[6] = 'e'; x[7] = 'e'; x[8] = 0;
	ArrayString y = new char[3];
	y[0] = 'a'; y[1] = 'b'; y[2] = 0;
	ArrayString z = new char[4];
	z[0] = 'x'; z[1] = 'y'; z[2] = 'z'; z[3] = 0;

	ReplaceString(x, y, z);

	std::cout << x << std::endl;
	std::cout << y << std::endl;
	std::cout << z << std::endl;

	delete[] x;
	delete[] y;
	delete[] z;

	return 0;
}

int Length(ArrayString &s)
{
	int count = 0;

	while (s[count] != 0) {
		count++;
	}

	return count;
}

void Append(ArrayString &s, char c)
{
	int oldLength = Length(s);
	ArrayString newStr = new char[oldLength + 1];

	for (int i = 0; i < oldLength; i++) {
		newStr[i] = s[i];
	}
	newStr[oldLength] = c;
	newStr[oldLength + 1] = 0;
	delete[] s;
	s = newStr;
}

void Concatenate(ArrayString &s1, ArrayString s2)
{
	int s1_OldLength = Length(s1);
	int s2_Length = Length(s2);
	int s1_NewLength = s1_OldLength + s2_Length;
	ArrayString newStr = new char[s1_NewLength + 1];

	for (int i = 0; i < s1_OldLength; i++) {
		newStr[i] = s1[i];
	}
	for (int i = 0; i < s2_Length; i++) {
		newStr[s1_OldLength + i] = s2[i];
	}
	newStr[s1_NewLength] = 0;
	delete[] s1;
	s1 = newStr;
}

ArrayString Substring(ArrayString &s, int startPos, int substrLen)
{

	int strLen = Length(s);
	int newStrLen;

	// Set the length of the new string to the requested substring length, or if the requested
	// substring length goes beyond the end of the string, set the length of the new string to
	// the start position plus whatever number of characters remain.
	if (strLen > startPos + substrLen)
		newStrLen = substrLen;
	else
		newStrLen = strLen;

	// Copy the substring into the new array
	ArrayString newStr = new char[newStrLen + 1];
	for (int i = startPos; i <= newStrLen; i++) {
		newStr[i - startPos] = s[i];
	}
	newStr[newStrLen] = 0;

	return newStr;
}

void ReplaceString(ArrayString &sourceStr, ArrayString &targetStr, ArrayString &replaceText)
{
	int sourceStrLen = Length(sourceStr);
	int targetStrLen = Length(targetStr);
	int replaceTextLen = Length(replaceText);

	for (int offset = 0; offset < sourceStrLen - targetStrLen; offset++) {
		if (Compare(sourceStr, targetStr, offset, targetStrLen)) {
			Replace(sourceStr, replaceText, offset, sourceStrLen, targetStrLen, replaceTextLen);
			offset += replaceTextLen;
			sourceStrLen += targetStrLen;
		}
	}
}


bool Compare(ArrayString &sourceStr, ArrayString &targetStr, int offset, int targetStrLen)
{
	for (int j = 0; j < targetStrLen; j++) {
		if (sourceStr[offset + j] != targetStr[j]) {
			return false;
		}
	}

	return true;
}

void Replace(ArrayString &sourceStr, ArrayString &replaceText, int offset, int sourceStrLen, int targetStrLen, int replaceTextLen)
{
	int newStrLen = sourceStrLen - targetStrLen + replaceTextLen;
	ArrayString newStr = new char[newStrLen + 1];

	// Copy characters before matching string
	for (int i = 0; i < offset; i++) {
		newStr[i] = sourceStr[i];
	}

	// Copy replacement text into new string
	for (int i = 0; i < replaceTextLen; i++) {
		newStr[offset + i] = replaceText[i];
	}

	// Determine offset in new and source strings from which to copy remaining characters
	int offsetNewStr = offset + replaceTextLen;
	int offsetSourceStr = offset + targetStrLen;

	// Copy characters after matching string
	for (int i = 0; i < sourceStrLen - offsetSourceStr; i++) {
		newStr[offsetNewStr + i] = sourceStr[offsetSourceStr + i];
	}

	// End of string marker
	newStr[newStrLen] = 0;

	// Cleanup old string
	delete sourceStr;

	// Set the source to the new string
	sourceStr = newStr;
}