#include <iostream>
#include "VLString.h"

int main()
{
	VLString v1;
	v1.Append('a');
	v1.Append('b');
	v1.Append('c');

	VLString v2;
	v2.Append('d');
	v2.Append('e');

	std::cout << "Append" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;

	std::cout << "[1] Operator" << std::endl;
	std::cout << v1[1] << std::endl;
	std::cout << v2[1] << std::endl;
	std::cout << std::endl;

	v1.Concatenate(v2);

	std::cout << "Concatenate" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;

	v2 = v1;

	std::cout << "Assignment" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;

	v1.Remove(1, 2);

	std::cout << "Deletion" << std::endl;
	v1.Print();
	v2.Print();
	std::cout << std::endl;


	return 0;
}


