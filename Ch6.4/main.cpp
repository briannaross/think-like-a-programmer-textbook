#include <iostream>

int MeanIter(const int array[], const int SIZE);
int MeanOccur(const int array[], const int SIZE, const int offset);

int main()
{
	const int SIZE = 10;
	const int array[SIZE] = { 812, 3, 5, 6, 5, 70009, 4, 1, 2, 5 };
	const int offset = 0;

	std::cout << "Iterative mean: " << MeanIter(array, SIZE) << std::endl;
	std::cout << "Recursive mean: " << MeanOccur(array, SIZE, offset) << std::endl;

	return 0;
}

int MeanIter(const int array[], const int SIZE)
{
	int sumOccurences = 0;

	for (int i = 0; i < SIZE; i++) {
		sumOccurences += array[i];
	}

	return sumOccurences /= SIZE;
}

int MeanOccur(const int array[], const int SIZE, const int offset)
{
	if (SIZE == offset) return 0;

	int sumOccurences = 0;

	sumOccurences = array[offset] + MeanOccur(array, SIZE, offset + 1);

	if (offset == 0) {
		sumOccurences /= SIZE;
	}

	return sumOccurences;
}
