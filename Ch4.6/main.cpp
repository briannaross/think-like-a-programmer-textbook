#include <iostream>

struct Character {
	char c;
	Character* next;
};

typedef Character* LLString;

void Append(LLString &str, char c);
char CharacterAt(LLString &str, int pos);
void Concatenate(LLString &s1, LLString &s2);

int main()
{
	// Initialisation
	LLString str;
	Character* c1 = new Character{ 'a' };
	Character* c2 = new Character{ 'b' };
	Character* c3 = new Character{ 'c' };
	c1->next = c2;
	c2->next = c3;
	c3->next = nullptr;
	str = c1;

	LLString str2;
	Character* c4 = new Character{ 'd' };
	Character* c5 = new Character{ 'e' };
	Character* c6 = new Character{ 'f' };
	c4->next = c5;
	c5->next = c6;
	c6->next = nullptr;
	str2 = c4;

	// Do some LLString manipulations
	Append(str, 'd');

	int pos = 4;
	std::cout << "Character at position " << pos << " is " << CharacterAt(str, pos) << std::endl;

	Concatenate(str, str2);

	// Output last rec
	Character* curRec = str;
	while (curRec != nullptr) {
		std::cout << curRec->c << std::endl;
		curRec = curRec->next;
	}

	// Cleanup
	curRec = str;
	while (curRec != nullptr) {
		Character* thisRec = curRec;
		curRec = curRec->next;
		delete thisRec;
	}
	curRec = str2;
	while (curRec != nullptr) {
		Character* thisRec = curRec;
		curRec = curRec->next;
		delete thisRec;
	}

	curRec = nullptr;
	str = nullptr;
	str2 = nullptr;
	c1 = nullptr;
	c2 = nullptr;
	c3 = nullptr;
	c4 = nullptr;
	c5 = nullptr;
	c6 = nullptr;



	return 0;
}


void Append(LLString &str, char c)
{
	Character* curRec = str;
	Character* newRec = new Character{ c };

	while (curRec->next != nullptr) {
		curRec = curRec->next;
	}

	newRec->next = nullptr;
	curRec->next = newRec;
}

char CharacterAt(LLString &str, int pos)
{
	Character* curRec = str;
	int recNum = 1;

	while (curRec != nullptr) {
		if (pos == recNum) {
			return curRec->c;
		}
		curRec = curRec->next;
		recNum++;
	}

	return ' ';
}

void Concatenate(LLString &s1, LLString &s2)
{
	Character* curRecS1 = s1;
	Character* curRecS2 = s2;

	while (curRecS1->next != nullptr) {
		curRecS1 = curRecS1->next;
	}

	while (curRecS2 != nullptr) {
		Character* newRec = new Character(*curRecS2);
		newRec->next = nullptr;

		curRecS1->next = newRec;
		curRecS1 = newRec;

		curRecS2 = curRecS2->next;
	}
}