#include <iostream>

bool AddNumbersIteratively(const int array[], const int SIZE);
int AddNumbersRecursively(const int array[], const int SIZE, int offset);

int main()
{
	const int SIZE = 10;
	const int array[SIZE] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	int offset = 0;

	std::cout << "Iterative parity: " << (AddNumbersIteratively(array, SIZE) ? "Even" : "Odd") << std::endl;
	std::cout << "Recursive parity: " << (AddNumbersRecursively(array, SIZE, offset) ? "Odd" : "Even") << std::endl;


	std::cout << AddNumbersRecursively(array, SIZE, offset) << std::endl;
	return 0;
}

bool AddNumbersIteratively(const int array[], const int SIZE)
{
	int sumEvenBits = 0;

	for (int i = 0; i < SIZE; i++) {
		if (array[i] == 1) {
			sumEvenBits++;
		}
	}

	if (sumEvenBits % 2 == 0) {
		return true;
	}

	return false;
}

int AddNumbersRecursively(const int array[], const int SIZE, int offset)
{
	if (SIZE == offset) return 0;

	return (array[offset] + AddNumbersRecursively(array, SIZE, offset + 1)) % 2;
}
