#include <iostream>

typedef char* ArrayString;

int Length(ArrayString &s);
void Append(ArrayString &s, char c);
void Concatenate(ArrayString &s1, ArrayString s2);
ArrayString Substring(ArrayString &s, int startPos, int length);
void ReplaceString(ArrayString &sourceString, ArrayString &targetString, ArrayString &replaceText);
bool Compare(ArrayString &sourceStr, ArrayString &targetStr, int offset, int replaceTextLen);
void Replace(ArrayString &sourceStr, ArrayString &replaceText, int offset, int sourceStrLen, int targetStrLen, int replaceTextLen);

int main()
{
	ArrayString a = new char[5];
	a[0] = 4; a[1] = 't'; a[2] = 'e'; a[3] = 's'; a[4] = 't';
	ArrayString c = new char[1];
	c[0] = 0;

	Concatenate(c, a);

	a = Substring(a, 0, 800);

	for (int i = 1; i <= a[0]; i++)
		std::cout << a[i];
	std::cout << std::endl;

	for (int i = 1; i <= c[0]; i++)
		std::cout << c[i];
	std::cout << std::endl;

	std::cout << (void*)a << "\n" << (void*)c << "\n";

	delete[] a;
	delete[] c;

	ArrayString x = new char[9];
	x[0] = 8; x[1] = 'a'; x[2] = 'b'; x[3] = 'c'; x[4] = 'd'; x[5] = 'a'; x[6] = 'b'; x[7] = 'e'; x[8] = 'e';
	ArrayString y = new char[3];
	y[0] = 2; y[1] = 'a'; y[2] = 'b';
	ArrayString z = new char[4];
	z[0] = 3; z[1] = 'x'; z[2] = 'y'; z[3] = 'z';

	ReplaceString(x, y, z);

	for (int i = 1; i <= x[0]; i++)
		std::cout << x[i];
	std::cout << std::endl;

	for (int i = 1; i <= y[0]; i++)
		std::cout << y[i];
	std::cout << std::endl;

	for (int i = 1; i <= z[0]; i++)
		std::cout << z[i];
	std::cout << std::endl;


	delete[] x;
	delete[] y;
	delete[] z;

	return 0;
}

int Length(ArrayString &s)
{
	int count = 0;

	while (s[count] != 0) {
		count++;
	}

	return count;
}

void Append(ArrayString &str, char c)
{
	int len = str[0];
	ArrayString newStr = new char[len + 1];

	// Copy old string to new string
	for (int i = 1; i <= len; i++) {
		newStr[i] = str[i];
	}

	// Append character to end of array
	newStr[len + 1] = c;
	
	// Increment length of string
	newStr[0] = len + 1;

	// Cleanup memory
	delete[] str;

	// Set s to the new string
	str = newStr;
}

void Concatenate(ArrayString &s1, ArrayString s2)
{
	int s1Len = s1[0];
	int s2Len = s2[0];
	int newStrLen = s1Len + s2Len;
	ArrayString newStr = new char[newStrLen + 1];

	for (int i = 1; i <= s1Len; i++) {
		newStr[i] = s1[i];
	}
	for (int i = 1; i <= s2Len; i++) {
		newStr[s1Len + i] = s2[i];
	}

	newStr[0] = newStrLen;

	delete[] s1;

	s1 = newStr;
}

ArrayString Substring(ArrayString &s, int startPos, int substrLen)
{
	int strLen = s[0];
	int newStrLen;

	// Set the length of the new string to the requested substring length, or if the requested
	// substring length goes beyond the end of the string, set the length of the new string to
	// the start position plus whatever number of characters remain.
	if (strLen >= startPos + substrLen)
		newStrLen = substrLen;
	else
		newStrLen = strLen;

	// Copy the substring into the new array
	ArrayString newStr = new char[newStrLen + 1];
	for (int i = startPos; i <= newStrLen; i++) {
		newStr[i - startPos] = s[i];
	}

	newStr[0] = newStrLen;

	delete s;

	return newStr;
}

void ReplaceString(ArrayString &sourceStr, ArrayString &targetStr, ArrayString &replaceText)
{
	int sourceStrLen = sourceStr[0];
	int targetStrLen = targetStr[0];
	int replaceTextLen = replaceText[0];

	for (int offset = 0; offset < sourceStrLen - targetStrLen; offset++) {
		if (Compare(sourceStr, targetStr, offset, targetStrLen)) {
			Replace(sourceStr, replaceText, offset, sourceStrLen, targetStrLen, replaceTextLen);
			offset += replaceTextLen;
			sourceStrLen = sourceStr[0];
		}
	}
}


bool Compare(ArrayString &sourceStr, ArrayString &targetStr, int offset, int targetStrLen)
{
	for (int j = 1; j <= targetStrLen; j++) {
		if (sourceStr[offset + j] != targetStr[j]) {
			return false;
		}
	}

	return true;
}

void Replace(ArrayString &sourceStr, ArrayString &replaceText, int offset, int sourceStrLen, int targetStrLen, int replaceTextLen)
{
	int newStrLen = sourceStrLen - targetStrLen + replaceTextLen;
	ArrayString newStr = new char[newStrLen + 1];

	// Copy characters before matching string
	for (int i = 1; i <= offset; i++) {
		newStr[i] = sourceStr[i];
	}

	// Copy replacement text into new string
	for (int i = 1; i <= replaceTextLen; i++) {
		newStr[offset + i] = replaceText[i];
	}

	// Determine offset in new and source strings from which to copy remaining characters
	int offsetNewStr = offset + replaceTextLen;
	int offsetSourceStr = offset + targetStrLen;

	// Copy characters after matching string
	for (int i = 1; i <= sourceStrLen - offsetSourceStr; i++) {
		newStr[offsetNewStr + i] = sourceStr[offsetSourceStr + i];
	}

	// End of string marker
	newStr[0] = newStrLen;

	// Cleanup old string
	delete sourceStr;

	// Set the source to the new string
	sourceStr = newStr;
}