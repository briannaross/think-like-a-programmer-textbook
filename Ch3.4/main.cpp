#include <iostream>
#include <string>
#include <time.h>

std::string EncodeText(std::string& originalText, const char cypher[], const int NUM_CHARS);
std::string DecodeText(std::string& encodedText, const char cypher[], const char codedCypher[], const int NUM_CHARS);

//std::string EncodeText(std::string& originalText, const char cypher[], const int NUM_CHARS, const int OFFSET);
//std::string DecodeText(std::string& encodedText, const char cypher[], const int NUM_CHARS, const int OFFSET);

int main()
{
	srand(time(NULL));

	const int NUM_CHARS = 26;
	const int OFFSET = 0;
	char cypher[NUM_CHARS] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	char codedCypher[NUM_CHARS] = {};
	//const char puncutation[8] = { '!', '?', '\'', '.', ' ', ';', '"', '`' };
	std::string originalText;
	std::string encodedText;
	std::string decodedText;


	for (size_t i = 0; i < NUM_CHARS; i++) {
		// Get a random element
		int c = rand() % 26;

		// While the coded cypher random element is not empty
		while (codedCypher[i] == '\0') {
			
			bool characterUsed = false;
			for (size_t j = 0; j < NUM_CHARS; j++) {
				if (codedCypher[j] == cypher[c]) {
					characterUsed = true;
					break;
				}
			}

			if (characterUsed == false) {
				codedCypher[i] = cypher[c];
			}

			c = rand() % 26;
			if (c == i)
				c = (c + 1) % NUM_CHARS;
		}
	}


	for (size_t i = 0; i < NUM_CHARS; i++) {
		std::cout << cypher[i] << " ";
	}
	std::cout << std::endl;
	for (size_t i = 0; i < NUM_CHARS; i++) {
		std::cout << codedCypher[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "Enter a string of text: ";
	char character = ' ';
	while (character != 10) {
		character = std::cin.get();
		originalText += character;
	}

	//encodedText = EncodeText(originalText, cypher, NUM_CHARS, OFFSET);
	encodedText = EncodeText(originalText, codedCypher, NUM_CHARS);
	std::cout << "Encoded text: " << encodedText << std::endl;

	//decodedText = DecodeText(encodedText, cypher, NUM_CHARS, OFFSET);
	decodedText = DecodeText(encodedText, cypher, codedCypher, NUM_CHARS);
	std::cout << "Decoded text: " << decodedText << std::endl;

	return 0;
}

std::string EncodeText(std::string& originalText, const char codedCypher[], const int NUM_CHARS)
{
	std::string encodedText;

	for (size_t i = 0; i < originalText.length(); i++) {
		if (originalText[i] >= 'A' && originalText[i] <= 'Z') {
			int cypherElem = (originalText[i] - 'A');
			encodedText += codedCypher[cypherElem];
		}
		else {
			encodedText += originalText[i];
		}
	}
	
	return encodedText;
}

std::string DecodeText(std::string& encodedText, const char cypher[], const char codedCypher[], const int NUM_CHARS)
{
	std::string decodedText;

	for (size_t i = 0; i < encodedText.length(); i++) {
		if (encodedText[i] >= 'A' && encodedText[i] <= 'Z') {
			int cypherElem;
			for (size_t j = 0; j < NUM_CHARS; j++) {
				if (codedCypher[j] == encodedText[i]) {
					cypherElem = j;
					break;
				}
			}
			decodedText += cypher[cypherElem];
		}
		else {
			decodedText += encodedText[i];
		}
	}

	return decodedText;
}

//std::string EncodeText(std::string& originalText, const char cypher[], const int NUM_CHARS, const int OFFSET)
//{
//	std::string encodedText;
//
//	for (size_t i = 0; i < originalText.length(); i++) {
//		if (originalText[i] >= 'A' && originalText[i] <= 'Z') {
//			int cypherElem = ((originalText[i] - 'A') + OFFSET) % NUM_CHARS;
//			encodedText += cypher[cypherElem];
//		}
//		else {
//			encodedText += originalText[i];
//		}
//	}
//	
//	return encodedText;
//}
//
//std::string DecodeText(std::string& encodedText, const char cypher[], const int NUM_CHARS, const int OFFSET)
//{
//	std::string decodedText;
//
//	for (size_t i = 0; i < encodedText.length(); i++) {
//		if (encodedText[i] >= 'A' && encodedText[i] <= 'Z') {
//			int cypherElem = ((encodedText[i] + 'A') - OFFSET) % NUM_CHARS;
//			decodedText += cypher[cypherElem];
//		}
//		else {
//			decodedText += encodedText[i];
//		}
//	}
//
//	return decodedText;
//}
