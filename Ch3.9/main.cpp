#include <iostream>

double arrayAverage(int intArray[], int ARRAY_SIZE);
int CompareFunc(const void* voidA, const void* voidB);

int main()
{
	const int NUM_AGENTS = 3;
	const int NUM_MONTHS = 12;
	int sales[NUM_AGENTS][NUM_MONTHS] = {
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 83097, 83098 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 13 },
		{ 79, 81, 81, 84, -1, -1, -1, -1, -1, -1, -1, -1 }
	};
	int highestMedian = -1;
	int agentWithHighestMedian = -1;

	// Sort the nested arrays to get sales from lowest to highest
	for (size_t agent = 0; agent < NUM_AGENTS; agent++) {
		qsort(sales[agent], NUM_MONTHS, sizeof(int), CompareFunc);
	}

	for (size_t agent = 0; agent < NUM_AGENTS; agent++) {
		for (size_t i = 0; i < NUM_MONTHS; i++)
		{
			std::cout << sales[agent][i] << " ";
		}
		std::cout << std::endl;
	}


	for (size_t agent = 0; agent < NUM_AGENTS; agent++) {
		int firstMonth = 0;
		int lastMonth = -1;
		int monthsWorked = 0;

		for (size_t i = 0; i < NUM_MONTHS; i++) {
			if (sales[agent][i] != -1) {
				firstMonth = i;
				break;
			}
		}

		for (size_t i = NUM_MONTHS - 1; i > 0; i--) {
			if (sales[agent][i] != -1) {
				lastMonth = i;
				break;
			}
		}

		if (lastMonth >= firstMonth) {
			monthsWorked = (lastMonth - firstMonth) + 1;
		}

		if (monthsWorked > 0) {
			int median = 0;
			std::cout << monthsWorked << std::endl;
			if (monthsWorked % 2 == 0) {
				median = (sales[agent][firstMonth + (monthsWorked / 2) - 1] + sales[agent][firstMonth + (monthsWorked / 2)]) / 2;
			}
			else {
				median = sales[agent][firstMonth + (monthsWorked / 2)];
			}

			if (median > highestMedian) {
				agentWithHighestMedian = agent;
				highestMedian = median;
			}
		}
	}

	if (highestMedian != -1)
		std::cout << "Highest median sales: " << highestMedian << " (Agent " << agentWithHighestMedian << ")\n";
	else
		std::cout << "No sales data to report on.\n";

	return 0;
}

double arrayAverage(int intArray[], int ARRAY_SIZE)
{
	double sum = 0;

	for (size_t i = 0; i < ARRAY_SIZE; i++) {
		sum += intArray[i];
	}
	double average = (sum + 0.5) / ARRAY_SIZE;

	return average;
}

int CompareFunc(const void* voidA, const void* voidB)
{
	// Cast pointers back to whatever I want, be it a scalar or non-scalar
	int* p1 = (int*)(voidA);
	int* p2 = (int*)(voidB);

	return *p1 - *p2;
}