#include <iostream>

int ConvertToDecimal(int baseFrom);
void ConvertFromDecimal(int baseTo, int num);

void FindDivisor(int &divisor, int num, int base);

int main()
{
	int elem = 0;
	int numWords = 0;
	int numVowels = 0;
	int curWordLen = 0;
	int lenLongestWord = 0;
	int mostVowelsInWord = 0;
	char character;

	std::cout << "Enter a line of text: ";
	character = std::cin.get();

	while (character != 10) {

		if (character == 'a' || character == 'A' || character == 'e' || character == 'E' || character == 'i' || character == 'I' || character == 'o' || character == 'O' || character == 'u' || character == 'U') {
			numVowels++;
		}

		if ((character >= 65 && character <= 90) || (character >= 97 && character <= 122)) {
			curWordLen++;
		}
		else if (curWordLen >= 1) {
			numWords++;
			if (curWordLen > lenLongestWord)
				lenLongestWord = curWordLen;
			if (numVowels > mostVowelsInWord)
				mostVowelsInWord = numVowels;
			curWordLen = 0;
			numVowels = 0;
		}


		character = std::cin.get();
	}

	if (curWordLen >= 1) {
		numWords++;
		if (curWordLen > lenLongestWord)
			lenLongestWord = curWordLen;
		if (numVowels > mostVowelsInWord)
			mostVowelsInWord = numVowels;
	}

	std::cout << "Number of words: " << numWords << std::endl;
	std::cout << "Most vowels in word: " << mostVowelsInWord << std::endl;
	std::cout << "Length of longest word: " << lenLongestWord << std::endl;

	return 0;
}
