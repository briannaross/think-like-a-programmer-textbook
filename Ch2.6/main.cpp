#include <iostream>

int main()
{
	int sum = 0;
	char digit = ' ';

	//std::cout << "Enter a number: ";
	//digit = std::cin.get();

	//while (digit != 10) {
	//	sum *= 2;
	//	sum += digit - '0';

	//	digit = std::cin.get();
	//}

	//std::cout << "Decimal conversion: " << sum << std::endl;

	int decimalNum = 0;
	int divisor = 1;

	std::cout << "Enter a number: ";
	std::cin >> decimalNum;

	while (divisor < decimalNum) {
		divisor *= 2;
	}

	if (divisor > decimalNum) {
		divisor /= 2;
	}

	std::cout << "Binary conversion: ";
	while (decimalNum > 0 || divisor > 0) {
		if (decimalNum >= divisor) {
			std::cout << 1;
			decimalNum -= divisor;
		}
		else
			std::cout << 0;
		
		divisor /= 2;
	}

	std::cin.get();

	return 0;
}