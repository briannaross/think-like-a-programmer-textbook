#pragma once
class MyNumber
{
public:
	MyNumber();
	~MyNumber();
	void AddNumber(int n);
	int SumIter();
	int SumRecur();

private:
	struct Number {
		int n;
		Number* next;
	};

	Number* _listHead;

	int PrivSumRecur(Number* pCurNum);
};

