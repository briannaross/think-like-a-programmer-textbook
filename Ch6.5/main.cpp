#include <iostream>
#include "MyNumber.h"

int main()
{
	MyNumber numbers;

	numbers.AddNumber(1);
	numbers.AddNumber(2);
	numbers.AddNumber(3);
	numbers.AddNumber(4);
	numbers.AddNumber(5);
	numbers.AddNumber(6);
	numbers.AddNumber(7);
	numbers.AddNumber(8);
	numbers.AddNumber(9);
	numbers.AddNumber(10);

	std::cout << "Iterative sum: " << numbers.SumIter() << std::endl;

	std::cout << "Recursive sum: " << numbers.SumRecur() << std::endl;

	return 0;
}
