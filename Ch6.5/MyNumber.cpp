#include "MyNumber.h"



MyNumber::MyNumber()
{
	_listHead = nullptr;
}


MyNumber::~MyNumber()
{
	Number* pCurNum = _listHead;

	while (pCurNum != nullptr) {
		Number* pThisNum = pCurNum;
		pCurNum = pCurNum->next;
		delete pThisNum;
	}

	_listHead = nullptr;
}

void MyNumber::AddNumber(int n)
{
	Number* pCurNum = _listHead;

	if (pCurNum == nullptr) {
		Number* pNewNum = new Number;
		pNewNum->n = n;
		pNewNum->next = nullptr;
		_listHead = pNewNum;
		return;
	}

	while (pCurNum->next != nullptr) {
		pCurNum = pCurNum->next;
	}

	Number* pNewNum = new Number;
	pNewNum->n = n;
	pNewNum->next = nullptr;
	pCurNum->next = pNewNum;
}

int MyNumber::SumIter()
{
	Number* pCurNum = _listHead;
	int sum = 0;

	while (pCurNum != nullptr) {
		sum += pCurNum->n;
		pCurNum = pCurNum->next;
	}

	return sum;
}

int MyNumber::SumRecur()
{
	Number* pCurNum = _listHead;

	return PrivSumRecur(pCurNum);
}

int MyNumber::PrivSumRecur(Number* pCurNum)
{
	if (pCurNum == nullptr) return 0;

	return pCurNum->n + PrivSumRecur(pCurNum->next);
}
