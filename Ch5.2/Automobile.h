#pragma once

#include <string>

class Automobile
{
public:
	Automobile();
	~Automobile();
	std::string GetManufacturer();
	std::string GetModel();
	int GetYear();
	void SetManufacturer(std::string &manufacturer);
	void SetModel(std::string &model);
	void SetYear(int year);
	std::string GetDescription();
	int GetAge();
private:
	std::string manufacturer;
	std::string model;
	int year;
};

