#include <iostream>
#include "Automobile.h"

int main()
{
	Automobile a1;

	std::string manufacturer = "Ford";
	std::string model = "Focus";

	a1.SetManufacturer(manufacturer);
	a1.SetModel(model);
	a1.SetYear(2004);

	std::cout << a1.GetDescription() << std::endl;
	std::cout << "Age: " << a1.GetAge() << std::endl;

	return 0;
}