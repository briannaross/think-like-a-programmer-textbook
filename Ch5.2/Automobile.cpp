#include <stdlib.h>
#include "Automobile.h"

Automobile::Automobile()
{
}

Automobile::~Automobile()
{
}

std::string Automobile::GetManufacturer()
{
	return manufacturer;
}

std::string Automobile::GetModel()
{
	return model;
}

int Automobile::GetYear()
{
	return year;
}

void Automobile::SetManufacturer(std::string &manufacturer)
{
	this->manufacturer = manufacturer;
}

void Automobile::SetModel(std::string &model)
{
	this->model = model;
}

void Automobile::SetYear(int year)
{
	this->year = year;
}

std::string Automobile::GetDescription()
{
	char strYear[5];

	_itoa_s(GetYear(), strYear, 5, 10);
	std::string description = " " + GetManufacturer() + " " + GetModel();

	return strYear + description;

}

int Automobile::GetAge()
{
	int currentYear = 2017;
	return currentYear - GetYear();
}
