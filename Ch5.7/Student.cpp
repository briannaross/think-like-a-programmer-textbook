#include <iostream>
#include "Student.h"


Student::Student()
{
	_listHead = nullptr;
}

Student::Student(Student &st)
{
	Record* curRec = st._listHead;

	while (curRec != nullptr) {
		AddStudent(curRec->studentID, curRec->grade, curRec->name);
		curRec = curRec->next;
	}
}


Student::~Student()
{
	DeleteList();
}

Student& Student::operator=(const Student &rhs)
{
	if (this != &rhs) {
		DeleteList();
		_listHead = CopiedList(rhs._listHead);
	}

	return *this;
}

void Student::AddStudent(int stuNum, int gr, std::string name)
{
	Record* newNode = new Record;
	newNode->name = name;
	newNode->studentID = stuNum;
	newNode->grade = gr;
	newNode->next = _listHead;
	_listHead = newNode;
}

void Student::DeleteStudent(int studentID)
{
	Record* curRec = _listHead;

	if (curRec->studentID == studentID) {
		_listHead = curRec->next;
		delete curRec;
		std::cout << "Record for student " << studentID << " deleted." << std::endl;
		return;
	}

	while (curRec->next != nullptr) {
		if (curRec->next->studentID == studentID) {
			Record* nextRec = curRec->next;
			curRec->next = curRec->next->next;
			delete nextRec;
			std::cout << "Record for student " << studentID << " deleted." << std::endl;
			return;
		}
		else {
			curRec = curRec->next;
		}
	}

	std::cout << "Student ID " << studentID << " not found. No records deleted." << std::endl;
	return;
}


void Student::PrintAverageScore()
{
	int count = 0;
	double sum = 0;
	Record* loopPtr = _listHead;

	while (loopPtr != nullptr) {
		sum += loopPtr->grade;
		count++;
		loopPtr = loopPtr->next;
	}

	double average = sum / count;

	std::cout << "Average score\n";
	std::cout << "=============\n";
	std::cout << average << "\n";
	std::cout << std::endl;
}

void Student::PrintGradeQuartiles()
{
	int gradeQuartiles[4] = { 0, 0, 0, 0 };

	Record* firstRec = _listHead;

	while (firstRec != nullptr) {
		if (firstRec->grade >= 75) {
			gradeQuartiles[GRADE_THREE]++;
		}
		else if (firstRec->grade >= 50) {
			gradeQuartiles[GRADE_TWO]++;
		}
		else if (firstRec->grade >= 25) {
			gradeQuartiles[GRADE_ONE]++;
		}
		firstRec = firstRec->next;
	}

	std::cout << "Grade quartiles\n";
	std::cout << "===============\n";
	for (size_t i = GRADE_ONE; i <= GRADE_THREE; i++) {
		std::cout << "Grade " << i + 1 << ": " << gradeQuartiles[i] << "\n";
	}
	std::cout << std::endl;
}

// Copy constructor
Student::Record* Student::CopiedList(const Record* original)
{
	// Return if no string to copy
	if (original == nullptr) {
		return nullptr;
	}

	// Create new list and copy first node from original list
	Record* newCharList = new Record;
	newCharList->grade = original->grade;
	newCharList->name = original->name;
	newCharList->studentID = original->studentID;

	// Get the next original list node
	Record *originalNode = original->next;

	// Prepare a new node for this list
	Record *newNode = newCharList;

	// For each original node, create a new node in this list and copy original node's data into it
	while (originalNode != nullptr) {
		newNode->next = new Record;
		newNode = newNode->next;
		newNode->grade = originalNode->grade;
		newNode->name = originalNode->name;
		newNode->studentID = originalNode->studentID;
		originalNode = originalNode->next;
	}

	// Set last node's next to null
	newNode->next = nullptr;

	// Return the new list
	return newCharList;
}


void Student::DeleteList()
{
	while (_listHead != nullptr) {
		Record* curRecord = _listHead;
		_listHead = _listHead->next;
		delete curRecord;
	}
}
