#include <iostream>
#include <string>
#include "Student.h"


int main()
{
	Student students;

	students.AddStudent(10001, 87, "Fred");
	students.AddStudent(10002, 28, "Tom");
	students.AddStudent(10003, 100, "Alistair");
	students.AddStudent(10004, 78, "Sasha");
	students.AddStudent(10005, 84, "Erin");
	students.AddStudent(10006, 98, "Belinda");
	students.AddStudent(10007, 75, "Leslie");
	students.AddStudent(10008, 70, "Candy");
	students.AddStudent(10009, 81, "Aretha");
	students.AddStudent(10010, 68, "Veronica");

	students.PrintAverageScore();

	Student s2(students);

	s2.PrintGradeQuartiles();

	Student s3 = s2;

	s3.DeleteStudent(10010);
	s3.PrintAverageScore();


	return 0;
}

