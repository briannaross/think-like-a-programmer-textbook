#pragma once
#include <string>

class Student
{
public:
	Student();
	Student(Student &st);
	~Student();

	Student& operator=(const Student &rhs);

	void AddStudent(int stuNum, int gr, std::string name);
	void DeleteStudent(int studentID);
	void PrintAverageScore();
	void PrintGradeQuartiles();

private:
	struct Record {
		int grade;
		int studentID;
		std::string name;
		Record* next;
	};

	Record * _listHead;

	void DeleteList();
	Record* CopiedList(const Record* original);

	enum {
		GRADE_ONE = 0,
		GRADE_TWO = 1,
		GRADE_THREE = 2,
	} grades;
};

