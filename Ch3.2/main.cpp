#include <iostream>

double arrayAverage(int intArray[], int ARRAY_SIZE);
int CompareFunc(const void* voidA, const void* voidB);

int main()
{
	const int NUM_AGENTS = 3;
	const int NUM_MONTHS = 12;
	int sales[NUM_AGENTS][NUM_MONTHS] = {
		{ 1856, 498, 30924, 87478, 328, 2653, 387, 3754, 387587, 2873, 276, 32},
		{ 5865, 5456, 3983, 6464, 9957, 4785, 3875, 3838, 4959, 1122, 7766, 2534},
		{ 23, 55, 67, 99, 265, 376, 232, 223, 4546, 564, 4544, 3434}
	};
	int highestMedian = 0;
	int agentWithHighestMedian = 0;

	// Sort the nested arrays to get sales from lowest to highest
	for (size_t agent = 0; agent < NUM_AGENTS; agent++) {
		qsort(sales[agent], NUM_MONTHS, sizeof(int), CompareFunc);
	}


	for (size_t agent = 0; agent < NUM_AGENTS; agent++) {
		int median = 0;
		if (NUM_MONTHS % 2 == 0) {
			median = (sales[agent][(NUM_MONTHS / 2) - 1] + sales[agent][NUM_MONTHS / 2]) / 2;
		}
		else {
			median = (sales[agent][NUM_MONTHS / 2] + sales[agent][(NUM_MONTHS / 2) + 1]) / 2;
		}

		if (median > highestMedian) {
			agentWithHighestMedian = agent;
			highestMedian = median;
		}
	}

	//for (size_t agent = 0; agent < NUM_AGENTS; agent++) {
	//	for (size_t month = 0; month < NUM_MONTHS; month++) {
	//		std::cout << sales[agent][month] << " ";
	//	}
	//	std::cout << std::endl;
	//}

	std::cout << "Highest median sales: " << highestMedian << " (Agent " << agentWithHighestMedian << ")\n";

	return 0;
}

double arrayAverage(int intArray[], int ARRAY_SIZE)
{
	double sum = 0;

	for (size_t i = 0; i < ARRAY_SIZE; i++) {
		sum += intArray[i];
	}
	double average = (sum + 0.5) / ARRAY_SIZE;

	return average;
}

int CompareFunc(const void* voidA, const void* voidB)
{
	// Cast pointers back to whatever I want, be it a scalar or non-scalar
	int* p1 = (int*)(voidA);
	int* p2 = (int*)(voidB);

	return *p1 - *p2;
}