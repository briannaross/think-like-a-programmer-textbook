#include <iostream>

int GetNumLeadingSpaces(int row, const int &numRows, int &spaces);

void main()
{
	const int size = 4;
	const int numRows = size + 4;
	int spaces = 0;

	for (int row = 1; row <= numRows; row++) {
		spaces = GetNumLeadingSpaces(row, numRows, spaces);

		for (int col = 1; col <= spaces; col++) {
			std::cout << " ";
		}

		if (row == 1 || row == numRows) {
			for (int col = 1; col <= size; col++) {
				std::cout << "#";
			}
		}
		else {
			std::cout << "#";
			for (int col = 1; col <= size + 2 - (spaces * 2); col++) {
				std::cout << " ";
			}
			std::cout << "#";
		}

		std::cout << "\n";
	}

	//for (int row = numRows; row >= 1; row--) {
	//	PrintPattern(row, numRows);
	//}

	std::cin.get();
}

int GetNumLeadingSpaces(int row, const int &numRows, int &spaces)
{
	if (row == 1 || row == numRows)
		return 2;
	else if (row == 2 || row == numRows - 1)
		return 1;
	else
		return 0;
}
