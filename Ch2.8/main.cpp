#include <iostream>

int ConvertToDecimal(int baseFrom);
void ConvertFromDecimal(int baseTo, int num);

void FindDivisor(int &divisor, int num, int base);

int main()
{
	int num = 0;
	char digit = ' ';
	int baseFrom = 0;
	int baseTo = 0;

	while (baseFrom <= 1 || baseFrom >= 10) {
		std::cout << "What base do you want to convert FROM? (2-9) ";
		baseFrom = (std::cin.get() - '0');
		std::cin.ignore(1);
		std::cin.clear();
	}

	while (baseTo <= 1 || baseTo >= 10) {
		std::cout << "What base do you want to convert TO? (2-9) ";
		baseTo = (std::cin.get() - '0');
		std::cin.ignore(1);
		std::cin.clear();
	}

	num = ConvertToDecimal(baseFrom);
	ConvertFromDecimal(baseTo, num);

	std::cout << std::endl;

	return 0;
}

int ConvertToDecimal(int baseFrom)
{
	char digit = ' ';
	int sum = 0;

	std::cout << "Enter a number : ";
	digit = std::cin.get();

	while (digit != 10) {
		sum *= baseFrom;
		sum += (digit - '0');

		digit = std::cin.get();
	}

	return sum;
}

void ConvertFromDecimal(int baseTo, int num)
{
	int divisor = 1;

	FindDivisor(divisor, num, baseTo);

	std::cout << "Base " << baseTo << " conversion: ";

	while (num > 0 || divisor > 0) {
		if (num >= divisor) {
			std::cout << num / divisor;
			num %= divisor;
		}
		else {
			std::cout << 0;
		}

		divisor /= baseTo;
	}
}

void FindDivisor(int &divisor, int num, int base)
{

	while (divisor < num) {
		divisor *= base;
	}

	if (divisor > num) {
		divisor /= base;
	}
}
