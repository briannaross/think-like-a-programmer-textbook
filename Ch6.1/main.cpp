#include <iostream>


int AddNumbersIteratively(int array[], int SIZE);
int AddNumbersRecursively(int array[], int SIZE, int offset);

int main()
{
	const int SIZE = 10;
	int array[SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	int offset = 0;

	std::cout << "Iterative sum: " << AddNumbersIteratively(array, SIZE) << std::endl;
	std::cout << "Recursive sum: " << AddNumbersRecursively(array, SIZE, offset) << std::endl;

	return 0;
}


int AddNumbersIteratively(int array[], int SIZE)
{
	int sum = 0;

	for (int i = 0; i < SIZE; i++) {
		sum += array[i];
	}

	return sum;
}

int AddNumbersRecursively(int array[], int SIZE, int offset)
{
	if (SIZE == offset) return 0;

	int sum = array[offset] + AddNumbersRecursively(array, SIZE, offset + 1);

	return sum;
}
